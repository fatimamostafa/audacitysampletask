package com.example.android.audacitysampletask;

import android.graphics.drawable.Drawable;




/**
 * Created by Fatima Mostafa on 16-Feb-17.
 */

public class PortfolioModel {

    Drawable drawable;
    String textAppTitle;
    String textContent;

    public PortfolioModel(Drawable drawable, String textAppTitle, String textContent) {
        this.drawable = drawable;
        this.textAppTitle = textAppTitle;
        this.textContent = textContent;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public String getTextAppTitle() {
        return textAppTitle;
    }

    public void setTextAppTitle(String textAppTitle) {
        this.textAppTitle = textAppTitle;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }
}
