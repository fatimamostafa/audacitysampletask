package com.example.android.audacitysampletask;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;




/**
 * A simple {@link Fragment} subclass.
 */
public class PortfolioFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    ArrayList<PortfolioModel> portfolioModels = new ArrayList<>();


    public PortfolioFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_portfolio, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_portfolio);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new PortfolioAdapter(portfolioModels, getActivity());

        portfolioModels.add(new PortfolioModel(ContextCompat.getDrawable(getActivity(),R.drawable.bg1), "HungryNaki! Android App", "Android"));
        portfolioModels.add(new PortfolioModel(ContextCompat.getDrawable(getActivity(),R.drawable.bg2), "Event App Concept", "UI/UX"));
        portfolioModels.add(new PortfolioModel(ContextCompat.getDrawable(getActivity(),R.drawable.bg3), "Rokomari Mobile App", "Android"));
        portfolioModels.add(new PortfolioModel(ContextCompat.getDrawable(getActivity(),R.drawable.bg7), "HungryNaki! Android App", "Android"));
        portfolioModels.add(new PortfolioModel(ContextCompat.getDrawable(getActivity(),R.drawable.bg5), "Event App Concept", "UI/UX"));
        portfolioModels.add(new PortfolioModel(ContextCompat.getDrawable(getActivity(),R.drawable.bg6), "Rokomari Mobile App", "Android"));
        portfolioModels.add(new PortfolioModel(ContextCompat.getDrawable(getActivity(),R.drawable.bg7), "HungryNaki! Android App", "Android"));
        portfolioModels.add(new PortfolioModel(ContextCompat.getDrawable(getActivity(),R.drawable.bg8), "Event App Concept", "UI/UX"));
        portfolioModels.add(new PortfolioModel(ContextCompat.getDrawable(getActivity(),R.drawable.bg9), "Rokomari Mobile App", "Android"));


        recyclerView.setAdapter(adapter);

        final FrameLayout frameLayout = (FrameLayout)view.findViewById(R.id.frame_layout);
        frameLayout.getBackground().setAlpha(0);
        final FloatingActionsMenu fabMenu = (FloatingActionsMenu)view.findViewById(R.id.fab_menu);
        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                frameLayout.getBackground().setAlpha(100);
                frameLayout.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        fabMenu.collapse();
                        return true;
                    }
                });
            }

            @Override
            public void onMenuCollapsed() {
                frameLayout.getBackground().setAlpha(0);
                frameLayout.setOnTouchListener(null);
            }
        });

        return view;
    }





}
