package com.example.android.audacitysampletask;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Fatima Mostafa on 16-Feb-17.
 */


public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioAdapter.ViewHolder> {


    private ArrayList<PortfolioModel> portfolioModels;
    private Context context;


    public PortfolioAdapter(ArrayList<PortfolioModel> portfolioModels, Context context){
        this.portfolioModels = portfolioModels;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_portfolio, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.imageApp.setImageDrawable(portfolioModels.get(position).getDrawable());
        holder.textAppTitle.setText(portfolioModels.get(position).getTextAppTitle());
        holder.textContent.setText(portfolioModels.get(position).getTextContent());
    }

    @Override
    public int getItemCount() {

        return portfolioModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        ImageView imageApp;
        TextView textAppTitle;
        TextView textContent;

        public ViewHolder(View itemView) {
            super(itemView);
            imageApp = (ImageView) itemView.findViewById(R.id.imageApp);
            textAppTitle = (TextView) itemView.findViewById(R.id.textAppTitle);
            textContent = (TextView) itemView.findViewById(R.id.textContent);
        }


    }
}
